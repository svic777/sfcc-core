# Welcome to the SiteGenesis Core Educational Repository

This repository contains the core of the SFCC project, as well as all the necessary components for full-fledged work (default config files, css, js, images etc.), so please use with caution.
We recommend starting any new educational projects based on this repository.

NOTE:**DO NOT CHANGE THIS REPO. USE IT ONLY FOR CLONE TO YOUR LOCAL/PERSONAL REPOSITORIES**

All further work in the educational project will take place in personal repositories [GitLab](https://gitlab.speroteck.com).

Check out our tagged versions if you'd like to have clean releases of SiteGenesis at [https://xchange.demandware.com/community/developer/site_genesis](https://xchange.demandware.com/community/developer/site_genesis).

## Best practice

1) **Don't change this repository** and use it only for clone to your personal repositories. 
2) **Do not change core cartridges** during your education in your personal repository (after clone) - use only custom cartridges for new features.

## Build tool
SiteGenesis supports [gulp](http://gulpjs.com) as build tool.

### Getting started
- Pull down the latest copy of SiteGenesis. If you're reading this doc, it is likely that you already have a version of SG with the build tool config.
- `cd` into the `gulp_builder` directory.
- Install node modules:
```sh
:; npm install  
```
This assumes that you already have `npm` installed on your command line. If not, please [install node](http://nodejs.org/download/) first.
If you encounter an error, please try and address that first, either by Googling or [contacting us](mailto:tnguyen@demandware.com).
- Install `gulp` (see below).

#### Installation
Install gulp globally
```sh
:; npm install -g gulp
```

Now that you have gulp and its dependencies installed, you can start using it in your workflow.

### Build project automatically
#### gulp_builder cartridge
Use gulp_builder cartridge to configure automatic building of the project.
All information about gulp configuration you can get here:
* Cartridge overview - [gulp_builder/gulp-builder-overview.md](./gulp_builder/gulp-builder-overview.md);
* Setting up the deployment project - [gulp_builder/README.md](./gulp_builder/README.md);
* Deployment Build Properties - [gulp_builder/DeployProperties.md](./gulp_builder/DeployProperties.md).

Also, the most important gulp configuration files are:
* builders.xml - describes all commands, tasks and command-line parameters;
* config.json - describes all tasks, cartridges, paths and used modules.

Always add new cartridges into appropriate section in config.json file like this (in other way your cartridge will not be visible for builder):
```json
  "sites": [
    {
      ...
      "cartridges": [
        "./my_new_cartridge",
        ...,
        "./app_storefront_controllers",
        "./app_storefront_core"
      ]
      ...
    }
  ],
```

#### Eclipse configuration
You need to have a properly configured Eclipse:
1) Window > Preferences > General > Workspace:
* Set "Build automatically";
* Set "Refresh using native hooks or polling";
* Set "Refresh on access";
* Set "Always close unrelated projects without prompt";
* Set "Text file encoding" to "Other" and choose "UTF-8";
* Set "New text file line delimiter" to "Other" and choose "Unix".
2) gulp_builder > Context menu "Properties" > "Builders" has new builders for each task.
You must edit each of them you want to use.
To specify watching files you want to build choose tab "Build options" click "Specify Resources..." and check source files.
To specify destination files choose tab "Refresh" click "Specify Resources..." and check default folder.

#### Deploy data configuration
1) First, you need to create new "Digital Server Connection" with proper options in Eclipse:
* Host name of sandbox (Example: `lyons2.evaluation.dw.demandware.net`);
* Sandbox user name and password data.
* On the next window you must choose **active** version directory;
* Last option - choose proper cartridges, **except** gulp_builder.
2) Need to make sure that the options in the config.json file (gulp_builder cartridge) are correctly written (like hostname of sandbox, but without instance name, see above - `.evaluation.dw.demandware.net`):
```json
  "deployment": {
      "instanceRoot": ".evaluation.dw.demandware.net",
      "stagingCertInstanceRoot": ".evaluation.dw.demandware.net",
      ...
    },
```
3) Based on an existing file _config-server_example.json_, create a new one **_config-server.json_** with such parameters:
```json
  {
      "deployment" : {
          "instances": "lyons2",
          "importInstances": "lyons2",
          "activationInstances": "lyons2",
          "user": "your_username",
          "password": "your_password"
      }
  }
```
* `instances`, `importInstances`, `activationInstances` values are selected from the host name (see above example - `lyons2`) and should be separate with comma (if there are several);
* `user`, `password` for sandbox.

_Note: File must not have comments._

Do this each time you pull new data_impex updates from repository

* From the command line, navigate to the "gulp_builder" folder.
* Run `gulp deploy-data --dataBundle="core-config"` throw your IDE or directly in command-line.

4) Alternate Usage on the command line (from within gulp_builder directory). Any of the options can be added to the command from the command line like so:
```
Ex. 1
gulp deploy-data --user="username" --password="password" --instances="lyons2" --importInstances="lyons2" --dataBundle="core-config"
Ex. 2
gulp deploy-data --user="username" --password="password" --instances="lyons2" --importInstances="lyons2" --dataBundle="core-config-data"
```
When used in this way, it will override any of the defaults from config.json.

### data_impex cartridge

This cartridge is designed to transmit parameters through "Site Import and Export" tool in BM.
Cartridge catalog structure completely repeats the structure of "Site Import and Export" (Data Units To Export tree).

#### Example of usage

1) In directory /sites of data_impex cartridge copy sub-directory SiteName and paste new one with name of your site in BM;
2) In new custom site directory you can configure your own site preferences, for example you could change your site settings by making changes into _site.xml_ file like this:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<site xmlns="http://www.demandware.com/xml/impex/site/2007-04-30" site-id="SiteName">
    <name>SiteName</name>
    <currency>USD</currency>
    <taxation>net</taxation>
    <custom-cartridges>your_cartridges:app_storefront_controllers:app_storefront_core</custom-cartridges>
    <storefront-status>online</storefront-status>
</site>
```
* `site-id="SiteName"` and `<name>SiteName</name>` change SiteName to your site name;
* `<custom-cartridges>your_cartridges:app_storefront_controllers:app_storefront_core</custom-cartridges>` your can add/delete cartridges in cartridge path;
* `<storefront-status>online</storefront-status>` you can change site status.

After task execution `gulp deploy-data`, you can verify that your site's settings have changed.
Alternative setup, for example, can be produced throw BM: Administration -> Sites -> Manage Sites -> Settings.
