'use strict';

/**
 * build-temp-client-js: Create temp directories for each project's svgs
 */
module.exports = function (gb) {

    gb.gulp.task('build-temp-svg', function () {
        var buildTempDir = require('../util/build-temp-dir'),
            builds = [];

        for (var i = 0; i < gb.sites.length; i++) {
            var paths = [];

            for ( var j = 0; j < gb.sites[i].cartridges.length; j++ ) {
                var path = gb.sites[i].cartridges[j] + '/cartridge/static/default/images/svg-icons/*.svg';

                if ( gb.sites[i].cartridges[j].split('/')[0] === '.') {
                    path = '../' + path;
                } else {
                    path = '../../' + path;
                }

                paths.push(path);
            }

            builds.push(buildTempDir(gb, {
                paths: paths,
                dirName: gb.workingPath + '/temp-svg-' + gb.sites[i].name
            }));
        }

        return gb.Q.all(builds);

    });

};
