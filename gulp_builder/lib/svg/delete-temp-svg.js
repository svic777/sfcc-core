'use strict';

/**
 * delete-temp-client-js: Remove the temporary directories from the working path
 */
module.exports = function (gb) {

    gb.gulp.task('delete-temp-svg', ['combine-svg'], function() {
        var del = require('del');
        return gb.gulp.src(gb.workingPath + '/temp-svg-*')
           .pipe(gb.vinylPaths(del));
    });

};
