'use strict';

/*
 * combine-svg: Combine the SVG files in the given path into one file
 */
module.exports = function (gb) {

    gb.gulp.task('combine-svg', ['build-temp-svg'], function () {

        // Stores the list of promises returned from building each projects SVG sprite images
        var svgSprite = require('gulp-svg-sprite'),
            fs = require('fs'),
            promises = [];

        // Compile the style icon SVG from the individual files
        var buildSVG = function (proj, dest) {

            var deferred = gb.Q.defer(),
                destinationPath = '../' + dest + '/cartridge',
                stream = gb.gulp.src(proj + '/*.svg')
                    .pipe(svgSprite({
                        mode: {
                            symbol: { // Activate the «view» mode
                                inline: true,
                                dest: './',
                                sprite: './static/default/images/compiled/sprites.svg',
                                bust: false,
                                render: {
                                    scss: {
                                        dest: './scss/default/compiled/_svg.scss'
                                    } // Activate Sass output
                                }
                            },
                            defs: {
                                inline: true,
                                dest: './',
                                sprite: './templates/default/components/svg.isml'
                            }
                        }
                    }))
                    .pipe(gb.gulp.dest(destinationPath));

            // When the stream has finished processing set the promise to resolved
            stream.on('end', function () {
                // Prepend the cache tag for the SVG ISML file
                var name = destinationPath + '/templates/default/components/svg.isml',
                    data = '<iscache status="on" type="relative" hour="168" />';
                fs.readFile(name, 'utf8', function(error, result) {
                    if (error && error.code !== 'ENOENT') {
                        console.log(error);
                        deferred.resolve();
                    } else {
                        if (result) {
                            data = data + '\n' + result;
                        }

                        fs.writeFile(name, data, function () {
                            deferred.resolve();
                        });
                    }
                });
            });

            return deferred.promise;

        };

        for ( var i = 0; i < gb.sites.length; i++ ) {
            promises.push(buildSVG(gb.workingPath + '/temp-svg-' + gb.sites[i].name, gb.sites[i].publicSVGs));
        }

        return gb.Q.all(promises);

    });

};
