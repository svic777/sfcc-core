'use strict';

module.exports = function (gb) {

    // Imports ZIP into DW server using HTTP posts
    gb.gulp.task('import-data', ['webdav-upload'], function() {

        console.log('Starting data import...');

        // ensure no other import is happening
        var isOkToImport = function (gb, instance) {
            const IMPORT_STATUS_RUNNING = 'Running';
            const IMPORT_PROCESS_NAME = 'Site Import';
            var deferred = gb.Q.defer();

            var httpOptions = {
                url: 'https://' + instance + gb.deployment.instanceRoot + '/on/demandware.store/Sites-Site/default/ViewSiteImpex-Status',
                jar: true,
                rejectUnauthorized: false
            };
            gb.request(
                    httpOptions,
                    function (error, response, body) {
                        var $ = gb.cheerio.load(body);
                        var isOkToImport = true;
                        $('.table_detail:contains(' + IMPORT_PROCESS_NAME + ')').each(function () {
                            if ($(this).parent().find(':contains(' + IMPORT_STATUS_RUNNING + ')').length > 0) {
                                isOkToImport = false;
                                deferred.reject(new Error('Previous import still in-process. Need to wait/retry.'));
                                return false;
                            }
                        });
                        if (isOkToImport) {
                            deferred.resolve();
                        }
                    });

            return deferred.promise;
        };

        var importFile = function (gb, instance, archiveFilename) {
            var deferred = gb.Q.defer();
            console.log('Importing file: ' + archiveFilename);
            var httpOptions = {
                url: 'https://' + instance + gb.deployment.instanceRoot + '/on/demandware.store/Sites-Site/default/ViewSiteImpex-Dispatch',
                form: {
                    ImportFileName: archiveFilename,
                    realmUse: 'false',
                    import: 'OK'
                },
                jar: true,
                rejectUnauthorized: false
            };

            gb.request.post(
                httpOptions,
                function (error, response) {
                    if (response.statusCode === 200) {
                        console.log('Successful import of ' + archiveFilename + ' on ' + instance);
                        deferred.resolve();
                        return deferred.promise;
                    } else {
                        console.log('Failed import of ' + archiveFilename + ' on ' + instance);
                        deferred.reject(new Error('Unknown Error: ' + response.statusCode));
                        return;
                    }
                }
            );

            return deferred.promise;
        };

        var importFiles = function (gb, instance) {
            gb.bmLogin(gb, instance).then(function() {
                // import each data zip file sequentially to avoid db lock conflicts
                return gb.deployment.archiveFilenames.reduce(function(promise, item) {
                    return promise.then(function() {
                        return gb.promiseRetry(function(retry) {
                            return isOkToImport(gb, instance).catch(retry);
                        });
                    }).then(function() {
                        return importFile(gb, instance, item);
                    });
                }, gb.Q());
            });
        };

        var instances = [];

        for (var i in gb.deployment.importInstances) {
            instances.push(gb.deployment.importInstances[i]);
        }

        var imports = [];

        for (var j in instances) {
            imports.push(importFiles(gb, instances[j], true));
        }

        // After imports are complete, record timestamp of deployment
        gb.fileSystem.writeFile(gb.lastDeploymentFileName, new Date().getTime(), (err) => {
            if (err) { throw err; }
        });

        return gb.Q.all(imports);

    });

};
